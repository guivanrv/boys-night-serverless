const AWS = require('aws-sdk');
const MOVIES_TABLE = "MovieDetails",
    MOVIES_PER_GENRE_TABLE = "MoviePerGenre2";

// uncomment to print queries and other debug stuff to console
AWS.config.logger = console;

const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-2' });

async function TableGetAll(tableName) {
    return new Promise((resolve, reject) => {
        const params = { TableName: tableName }
        docClient.scan(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                // continue scanning if we have more users, because
                // scan can retrieve a maximum of 1MB of data
                // TODO: gotta test it later, lol
                if (typeof data.LastEvaluatedKey != "undefined") {
                    reject(new Error(`More than 1MB of ${tableName}!`));
                }
                // print all the movies
                resolve(data.Items);
            };
        });
    })
}
async function TableGetPage(tableName, partOfId, lastEvaluatedKey) {
    return new Promise((resolve, reject) => {
        let params = {
            TableName: tableName,
        };
        if (partOfId !== '*') {
            params = {
                TableName: tableName,
                FilterExpression: "begins_with(#id, :partOfId)",
                ExpressionAttributeNames: {
                    "#id": "dynamo",
                },
                ExpressionAttributeValues: {
                    ":partOfId": partOfId
                }
            }
        }
        if (lastEvaluatedKey) {
            params.LastEvaluatedKey = lastEvaluatedKey;
        }
        docClient.scan(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve({
                    lastEvaluatedKey: data.LastEvaluatedKey,
                    items: data.Items
                });
            };
        });
    })
}

async function TableGet(tableName, id, idParamName = "id") {
    return new Promise((resolve, reject) => {
        const params = {
            TableName: tableName, Key: {
                [idParamName]: id
            }
        }
        docClient.get(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data.Item);
            };
        });
    })
}

function sendErrorMsg(callback, error, entityName = 'entity') {
    console.error(error);
    callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true },
        body: `Couldn\'t list the ${entityName}`,
    });
}

function sendResponse(callback, obj) {
    callback(null, {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true },
        body: JSON.stringify(obj, null, 2),
    })
}

module.exports = {
    async getAllMovies(event, context, callback) {
        try {
            const movies = await TableGetAll(MOVIES_TABLE);
            sendResponse(callback, movies);
        } catch (error) {
            sendErrorMsg(callback, error, 'movies');
        }
    },

    async getMovieByGenre(event, context, callback) {
        try {
            const genre = event.pathParameters.genre;
            const lastId = event.pathParameters.lastId;
            const response = await TableGetPage(MOVIES_PER_GENRE_TABLE, genre, 20, lastId);
            sendResponse(callback, response);

        } catch (error) {
            sendErrorMsg(callback, error, 'movie');
        }
    },

    async getMovie(event, context, callback) {
        try {
            const id = event.pathParameters.id;
            const movie = await TableGet(MOVIES_TABLE, Number(id));
            sendResponse(callback, movie);

        } catch (error) {
            sendErrorMsg(callback, error, 'movie');
        }
    },

    async getMovieShort(event, context, callback) {
        try {
            const id = event.pathParameters.id;
            const movieShort = TableGet(MOVIES_PER_GENRE_TABLE, id, "dynamo");
            sendResponse(callback, movieShort);
        } catch (error) {
            sendErrorMsg(callback, error, 'movie-short');
        }
    },
};